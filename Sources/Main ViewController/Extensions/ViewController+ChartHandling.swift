//
//  ViewController+ChartHandling.swift
//  AmazingPlotApp
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import UIKit

import Charts
import SmoothingKit

//MARK: Line chart handling methods
extension ViewController {
    
    internal func reloadData() {
        
        //LABEL
        self.dataGeneratedCountLabel.text = "\(self.dataGeneratedCount) data generated (\(self.currentTimestamp)\")"
        
        //reload UI
        self.render()
        
        //PLOT
        var dataSets : [IChartDataSet] = []
        
        let timestamps = self.accelerationsValues[self.axisNameOfTimestamps]!
        
        
        for (axisName, isEnabled) in self.accelerationsAxes {
            
            if isEnabled {
                
                var yValsDataSet : [ChartDataEntry] = []
                
                if let accelerationsValuesForAccelerationAxis = self.accelerationsValues[axisName],
                    !accelerationsValuesForAccelerationAxis.isEmpty {
                    
                    var i = 0
                    
                    for accelerationValue in accelerationsValuesForAccelerationAxis {
                        
                        if axisName == self.axisNameSmoothedPeaksChannel {
                            
                            if Double(accelerationValue) > DetectPeaksOperator.marker {
                                
                                yValsDataSet.append(ChartDataEntry(x: Double(timestamps[i]),
                                                                   y: Double(accelerationValue) + (Double(accelerationValue) < 1.0 ? 0.04 : (Double(accelerationValue) < 2.0 ? 0.1 : 0.3))))
                                
                            }
                            
                        } else {
                            
                            yValsDataSet.append(ChartDataEntry(x: Double(timestamps[i]),
                                                               y: Double(accelerationValue)))
                            
                        }
                        
                        i += 1
                        
                    }
                    
                }
                
                let dataSet = LineChartDataSet(values: yValsDataSet,
                                               label: axisName)
                
                dataSets.append(self.setLineChartDataSetProperties(to : dataSet,
                                                                   and: axisName))
                
            }
            
        }
        
        //avoid further UI updated if no data given
        if dataSets.isEmpty { return }
        
        
        //feed plot with new data
        self.lineChart.data = LineChartData(dataSets: dataSets)
        
        //lock the plot to diplay a window of data
        self.lineChart.setVisibleXRangeMaximum(self.visibleXRangeMaximum())
        
        
        //window of data, which is the last one if self.isRealTime
        if self.currentMode == .RealtimeMode {
            
            self.lineChart.moveViewToX(Double(timestamps.last!) - self.visibleXRangeMaximum())
            
        }
        
    }
    
    fileprivate func visibleXRangeMaximum() -> Double {
    
        return Double(self.XValuesToShow) * self.UIRefreshRate * 0.99
    
    }
    
    internal func setupChartView() {
        
        self.lineChart.chartDescription?.text = ""
        self.lineChart.pinchZoomEnabled = true
        self.lineChart.drawGridBackgroundEnabled = false
        self.lineChart.scaleXEnabled = true
        self.lineChart.scaleYEnabled = false
        self.lineChart.minOffset = 20
        self.lineChart.autoScaleMinMaxEnabled = true
        self.lineChart.noDataText = ""
        self.lineChart.highlightPerDragEnabled = false
        self.lineChart.doubleTapToZoomEnabled = false
        self.lineChart.legend.enabled = false
        self.lineChart.delegate = self
        self.lineChart.setVisibleXRangeMaximum(self.visibleXRangeMaximum())
        self.lineChart.dragDecelerationEnabled = false
        
        
        
        let xAxis = self.lineChart.xAxis
        
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.drawLabelsEnabled = true
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.labelTextColor = UIColor.yellow
        xAxis.labelFont = UIFont.systemFont(ofSize: 8.0)
        xAxis.labelPosition = .bottom
        xAxis.valueFormatter = DefaultAxisValueFormatter(formatter: TimeIntervalNumberFormatter())
        
        let leftAxis = self.lineChart.getAxis(YAxis.AxisDependency.left)
        
        leftAxis.drawGridLinesEnabled = false
        leftAxis.drawLabelsEnabled = true
        leftAxis.drawAxisLineEnabled = false
        leftAxis.labelTextColor = UIColor.yellow
        leftAxis.labelFont = UIFont.systemFont(ofSize: 8.0)
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: AccelerationNumberFormatter())
        
        let rightAxis = self.lineChart.getAxis(YAxis.AxisDependency.right)
        
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawAxisLineEnabled = false
        
    }
    
    fileprivate func setLineChartDataSetProperties(to dataSet : LineChartDataSet,
                                                   and axisName: String) -> LineChartDataSet {
        
        dataSet.setColor(self.accelerationsAxesColor[axisName]!)
        dataSet.setCircleColor(self.accelerationsAxesColor[axisName]!)
        dataSet.lineWidth = 1.0
        dataSet.drawCirclesEnabled = false
        dataSet.circleHoleColor = UIColor.black
        dataSet.drawCircleHoleEnabled = false
        dataSet.fillAlpha = 1.0
        dataSet.drawFilledEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.valueTextColor = UIColor.white
        dataSet.highlightColor = UIColor.red
        dataSet.drawHorizontalHighlightIndicatorEnabled = false
        dataSet.highlightLineWidth = 3.0
        
        if axisName == self.axisNameSmoothedPeaksChannel {
            
            dataSet.drawCirclesEnabled = true
            dataSet.lineWidth = 0.0
            dataSet.circleRadius = 5.0
            
        }
        
        return dataSet
        
    }
    
}

//MARK: ChartViewDelegate implementation
extension ViewController : ChartViewExtendedDelegate {
    
    func panGestureEnded(_ chartView: ChartViewBase, direction: UIPanGestureRecognizerDirection, minXVisibile: Double) {

        print("visible window has to be locked to: \(minXVisibile)")
    }
    
    func panGestureBegan(_ chartView: ChartViewBase, direction: UIPanGestureRecognizerDirection, minXVisibile: Double) {
        
        if direction != .Right { return }
        
        if self.currentMode == .ReviewMode { return }
            
            //TODO: add a check to let review mode only if drag to left
            
            self.currentMode = .ReviewMode
            
    }
    
}
