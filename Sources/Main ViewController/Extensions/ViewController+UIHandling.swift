//
//  ViewController+UIHandling.swift
//  AmazingPlotApp
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import UIKit

//MARK: UI handling
extension ViewController {
    
    internal func render() {
        
        self.view.backgroundColor = UIColor.black
        
        var viewsHeight = self.calculateViewsHeights()
        
        //dataGeneratedCountLabel
        self.dataGeneratedCountLabel.frame = CGRect(x: 0,
                                                    y: self.topPadding,
                                                    width: self.view.frame.width,
                                                    height: viewsHeight[self.dataGeneratedCountLabel]!)
        
        self.dataGeneratedCountLabel.textAlignment = .center
        
        self.dataGeneratedCountLabel.font = UIFont.systemFont(ofSize: 8.0)
        
        self.dataGeneratedCountLabel.textColor = UIColor.yellow
        
        //lineChart UX
        self.lineChart.frame = CGRect(x: 10,
                                      y: self.dataGeneratedCountLabel.frame.maxY,
                                      width: self.view.frame.width - 10 - 10,
                                      height: viewsHeight[self.lineChart]!)
        
        
        //realTimeModeButton UX
        self.realTimeModeButton.frame = CGRect(x: 10,
                                               y: self.lineChart.frame.maxY,
                                               width: self.lineChart.frame.width,
                                               height: viewsHeight[realTimeModeButton]!)
        
        self.realTimeModeButton.backgroundColor = UIColor.clear
        
        self.realTimeModeButton.isEnabled = self.currentMode == .ReviewMode
        
        self.realTimeModeButton.setTitle("< Back to RealTime", for: .normal)
        
        self.realTimeModeButton.setTitle("", for: .disabled)
        
        self.realTimeModeButton.setTitleColor(UIColor.yellow, for: .normal)
        
        
        //axisSwitch
        let switchsHeight = viewsHeight[self.axesSwitches[self.sortedAxesNameForSwitches.first!]!]!
        
        let externalPadding : CGFloat = 15.0
        
        let widthForSwitch : CGFloat = 51.0
        
        let innerPadding : CGFloat = (self.view.frame.width - 2 * externalPadding - CGFloat(self.axesSwitches.count) * widthForSwitch) / CGFloat(self.axesSwitches.count - 1)
        
        var placedSwitch = 0
        
        for axisName in self.sortedAxesNameForSwitches {
            
            let axisSwitch = self.axesSwitches[axisName]!
            
            let lastXOrigin = externalPadding + CGFloat(placedSwitch) * (innerPadding + widthForSwitch)
            
            axisSwitch.frame = CGRect(x: lastXOrigin,
                                      y: self.realTimeModeButton.frame.maxY,
                                      width: widthForSwitch,
                                      height: switchsHeight)
            
            axisSwitch.thumbTintColor = self.accelerationsAxesColor[axisName]
            
            axisSwitch.tintColor = UIColor.yellow
            
            placedSwitch += 1
            
        }
        
    }
    
    internal func applyCurrentMode() {
        
        self.lineChart.highlightPerTapEnabled = self.currentMode == .ReviewMode
        
        self.lineChart.autoScaleMinMaxEnabled = self.currentMode == .RealtimeMode
        
        self.lineChart.resetZoom()
        
    }
    
    private func calculateViewsHeights() -> [UIView : CGFloat] {
        
        var viewsHeight : [UIView : CGFloat] = [self.dataGeneratedCountLabel : 20.0,
                                                self.realTimeModeButton : 30.0,
                                                self.axesSwitches[self.sortedAxesNameForSwitches.first!]! : 50.0,
                                                self.lineChart : self.view.frame.height]
        
        for (view, height) in viewsHeight {
            
            if view != self.lineChart {
                
                viewsHeight[self.lineChart] = viewsHeight[self.lineChart]! - height
            }
            
        }
        
        viewsHeight[self.lineChart] = viewsHeight[self.lineChart]! - self.topPadding
        
        return viewsHeight
        
    }
    
}

//MARK: User interactions handling methods
extension ViewController {
    
    dynamic func toggleRealTimeMode () {
        
        self.currentMode = .RealtimeMode
        
//        print("toggleRealTimeMode")
        
    }
    
    dynamic func toggleAxis (sender : UISwitch) {
        
        var axisName : String?
        
        var onSwitchesCount = 0
        
        for (name, uiSwitch) in self.axesSwitches {
            
            if uiSwitch.isOn { onSwitchesCount += 1 }
            
            if uiSwitch == sender {
                
                axisName = name
                
            }
            
        }
        
        if onSwitchesCount > 0 {
            
            if let axisName = axisName {
                
                self.accelerationsAxes[axisName] = sender.isOn
                
            }
            
            self.accelerationsAxes[self.axisNameSmoothedPeaksChannel] = self.accelerationsAxes[self.axisNameSmoothedChannel]
            
            return
            
        }
        
        //avoid to hide all accelerations channels
        for (name, uiSwitch) in self.axesSwitches {
            
            if uiSwitch != sender {
                
                uiSwitch.isOn = true
                
                self.accelerationsAxes[name] = true
                
            }
            
        }
        
        self.accelerationsAxes[self.axisNameSmoothedPeaksChannel] = self.accelerationsAxes[self.axisNameSmoothedChannel]
    
    }
    
    internal func setupSwitches() {
        
        for (name, uiSwitch) in self.axesSwitches {
            
            uiSwitch.isOn = self.accelerationsAxes[name]!
            
        }
        
    }
    
}

