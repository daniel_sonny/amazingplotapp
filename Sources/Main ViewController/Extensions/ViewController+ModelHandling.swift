//
//  ViewController+ModelHandling.swift
//  AmazingPlotApp
//
//  Created by Daniel Sonny Agliardi on 24/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation

import SmoothingKit

fileprivate let gravity : Float = 9.81

//MARK: Model data generation
extension ViewController {
    
    fileprivate func accelerationRange() -> (max: Float, min: Float) {
    
        return (min: -5.0, max: 5.0)
        
    }
    
    internal func generateNewData() {
        
        self.dataGeneratedCount += 1
        
        self.currentTimestamp = Float(self.dataGeneratedCount) * Float(self.dataGenerationRate)
        
        for axisName in ViewController.axesNames { 
            
            if axisName == self.axisNameSmoothedChannel { continue }
            
            if axisName == self.axisNameSmoothedPeaksChannel { continue }
            
            if axisName == self.axisNameOfTimestamps {
                
                var mutableAccelerationsValues = self.accelerationsValues[axisName]!
                
                mutableAccelerationsValues.append(self.currentTimestamp)
                
                self.accelerationsValues[axisName] = mutableAccelerationsValues
                
                continue
                
            }
            
            if axisName != self.axisNameToSmooth {
            
            //code to generete a new random acceleration value
            var mutableAccelerationsValues = self.accelerationsValues[axisName]!
            
            mutableAccelerationsValues.append(Float.random(min: accelerationRange().min, max: accelerationRange().max))
            
            self.accelerationsValues[axisName] = mutableAccelerationsValues
            
            }
            else { //if axisName is equal to axisNameToSmooth, than create even smothed version
                
                ///code to append a new Yvalue from recordedYValues
                var mutableAccelerationsValues = self.accelerationsValues[axisName]!
                
                let indexOfRecordedValue = self.dataGeneratedCount % (self.recordedYValuesInMS.count)
                
                let recordedValue = self.recordedYValuesInMS[indexOfRecordedValue] * gravity
                
                mutableAccelerationsValues.append(recordedValue)
                
                self.accelerationsValues[axisName] = mutableAccelerationsValues
                
                //////////
                let arrayToSmoot = self.accelerationsValues[axisName]!
                    
                //creating SmoothedChannel
                let filteredArrayToSmoot = self.filterOperator.apply(onFloatArray : arrayToSmoot)
                
                let smoothedChannel = self.smoothingOperator.apply(onFloatArray: filteredArrayToSmoot)
                
                //creating SmoothedPeaksChannel
                let smoothedPeaksChannel = DetectPeaksOperator.apply(onFloatArray: smoothedChannel,
                                                                     andTimestamps: self.accelerationsValues[self.axisNameOfTimestamps]!).y
                
                self.accelerationsValues[self.axisNameSmoothedChannel] = smoothedChannel
                
                self.accelerationsValues[self.axisNameSmoothedPeaksChannel] = smoothedPeaksChannel
                
            }
            
        }
        
    }
    
    public static func loadYValuesFromFile() -> [Float] {
        
        var YValues : [Float] = []
        
        if let path = Bundle.main.path(forResource: ViewController.axesNames[2], ofType: "plist") {
            
            if let rawArray = NSArray(contentsOfFile: path),
                rawArray.count > 0 {
                
                for rawvalue in rawArray {
                    
                    if let rawvalue = rawvalue as? Double {
                        
                        YValues.append(Float(rawvalue))
                        
                    }
                    
                }
                
            }
            
        }
        
        return YValues
        
    }
    
}
