//
//  ViewController.swift
//  AmazingPlotApp
//
//  Created by Daniel Sonny Agliardi on 22/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import UIKit

import Charts
import SmoothingKit

class ViewController : UIViewController {
    
    internal enum AmazingPlotMode {
        
        case RealtimeMode
        
        case ReviewMode
        
        var description : String {
            
            switch self {
                
            case .RealtimeMode:
                return "RealtimeMode"
                
            case .ReviewMode:
                return "ReviewMode"
            }
            
        }
        
    }
    
    //MARK: defines
    let dataGenerationRate : TimeInterval = 1/50
    
    var UIRefreshRate : TimeInterval = 1/5
    
    let XValuesToShow = 10
    
    
    //MARK: Status properties
    var currentMode : AmazingPlotMode = .RealtimeMode {
        
        didSet {
            
//            print("\(oldValue.description)->\(self.currentMode.description)")
            
            if oldValue != self.currentMode {
                
                self.applyCurrentMode()
                
            }
            
        }
        
    }
    
    var dataGeneratedCount = 0
    
    
    //MARK: timers
    var dataGenerationTimer : Timer?
    
    var UIRefreshTimer : Timer?
    
    
    //MARK: UI objects
    let topPadding : CGFloat = 25.0
    
    let dataGeneratedCountLabel = UILabel()
    
    let realTimeModeButton = UIButton()
    
    let lineChart = LineChartView()
    
    let axesSwitches = ["X": UISwitch(),
                        "Y": UISwitch(),
                        "Z": UISwitch(),
                        "smoothedY" : UISwitch()]
    
    let sortedAxesNameForSwitches = ["X", "Y", "Z", "smoothedY"]
    
    
    //MARK: Model objects
    var currentTimestamp : Float = 0.0
    
    let smoothingOperator = SmoothingOperator(nl: 3, nr: 3)
    
    let filterOperator = LowPassFilter()
    
    let axisNameOfTimestamps = "timestamp"
    
    let axisNameSmoothedChannel = "smoothedY"
    
    let axisNameSmoothedPeaksChannel = "smoothedPeaksY"
    
    let axisNameToSmooth = "Y"
    
    internal static let axesNames = ["timestamp", "X", "Y", "Z", "smoothedY", "smoothedPeaksY"]
    
    let recordedYValuesInMS = ViewController.loadYValuesFromFile()
    
    var accelerationsValues = ["X" : [Float](),
                               "Y" : [Float](),
                               "Z" : [Float](),
                               "smoothedY" : [Float](),
                               "smoothedPeaksY" : [Float](),
                               "timestamp" : [Float]()]
    
    var accelerationsAxes = ["X" : true,
                             "Y" : true,
                             "Z" : true,
                             "smoothedY" : true,
                             "smoothedPeaksY" : true]
    
    var accelerationsAxesColor = ["X" : UIColor.red,
                                  "Y" : UIColor(red:0.42, green:0.66, blue:0.45, alpha:1.0),
                                  "Z" : UIColor.blue,
                                  "smoothedPeaksY" : UIColor.orange,
                                  "smoothedY" : UIColor.orange]
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        //Init model
        for axisName in self.accelerationsValues.keys {
            
            self.accelerationsValues[axisName] = [Float](repeating: 0.0, count: self.XValuesToShow)
            
        }
        
        
        self.setupChartView()
        
        self.setupSwitches()
        
        self.applyCurrentMode()
        
        
        for axisSwitch in self.axesSwitches.values {
            
            axisSwitch.addTarget(self, action: #selector(toggleAxis), for: .valueChanged)
            
            self.view.addSubview(axisSwitch)
            
        }
        
        self.view.addSubview(self.lineChart)
        
        self.view.addSubview(self.realTimeModeButton)
        
        self.view.addSubview(self.dataGeneratedCountLabel)
        
        
        self.realTimeModeButton.addTarget(self, action: #selector(toggleRealTimeMode), for: .touchUpInside)
        
        
        self.dataGenerationTimer = Timer.scheduledTimer(withTimeInterval: self.dataGenerationRate, repeats: true, block: { _ in
            
            self.generateNewData()
            
        })
        
        self.UIRefreshTimer = Timer.scheduledTimer(withTimeInterval: self.UIRefreshRate, repeats: true, block: { _ in
            
            self.reloadData()
            
        })
        
    }
    
    override func viewDidLayoutSubviews() { self.render() }
    
    
    
    deinit {
        
        self.dataGenerationTimer?.invalidate()
        
        self.UIRefreshTimer?.invalidate()
        
    }
    
}
