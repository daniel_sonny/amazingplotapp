//
//  NumberFormatters.swift
//  AmazingPlotApp
//
//  Created by Daniel Sonny Agliardi on 23/07/2017.
//  Copyright © 2017 DSA. All rights reserved.
//

import Foundation

class AccelerationNumberFormatter : NumberFormatter {
    
    override func string(from number: NSNumber) -> String? {
        
        let seconds = number.floatValue
        
        if modf(seconds).1 == 0.0 {
            
            return String(format: "%.0f g", seconds)
            
        }
        
        return String(format: "%.1f g", seconds)
        
    }
    
}

class TimeIntervalNumberFormatter : NumberFormatter {
    
    override func string(from number: NSNumber) -> String? {
        
        let seconds = number.floatValue
        
        if modf(seconds).1 == 0.0 {
            
            return String(format: "%.0f\"", seconds)
            
        }
        
        return String(format: "%.2f\"", seconds)
        
    }
    
}
