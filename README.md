# README #

### Purpose ###

This repo contains the result of homework given to me, by [Empatica Inc.](https://www.empatica.com/), as a proof of my skill in software engineering, mobile development and data analysis.

_This README is intended as full documentation that cover all assignment points._



### Explanation ###

AmazingPlotApp is an iOS app designed to plot a 3-axial signal of acceleration (i. e. a signal from sensor connected via bluetooth to the phone) in a human-readable way.

The app is composed by:

  * A simple iOS with a single ViewController;
  * A mathematical and signal processing Xcode framework, called SmoothingKit, written by me;
  * One of most complex framework to plot any kind of data available on Apple platform, [Charts](https://github.com/danielgindi/Charts) by [Daniel Cohen Gindi](https://github.com/danielgindi).



##### Main App #####

As the screenshot shows, I decided to keep the UI/UX simple, clean and intuitive, as possible to the meet all requirements.

![A screenshot from the app](https://i.imgur.com/Vau7TyL.png | width=200)

The app plots 3-axial acceleration signal that has 50Hz as sampling frequency. The acceleration channels follow those rules:

  * "x axis" (red) and "z axis" (blue) are values generated random in a realistic range (from -9.0g to 9.0g).
  * "y axis" is a recorded (with a [Beast sensor](https://www.thisisbeast.com/en)) sampling of a [Squat](https://en.wikipedia.org/wiki/Squat_(exercise)) execute at the gym for the first 4000 value, than the values are generated random in a realistic range (from -9.0g to 9.0g).
  * "smoothed y axis" (orange) is a channel that contains processed values of "y axis". As requested, the process consist in:

    Butterworth filter (low pass filter to get only low frequency peaks) -> Savitzky-Golay filter (for the smoothing) -> extractions of peaks.

The user can hide a specific channel on line plot, by toggling the related switch.

As described in requirements, the app has 2 mode:

  * "live mode", in this mode the visible window of the plot is locked to the right side to show the fresh values as soon they are collected. The user can, seamless, switch to "review mode" by scrolling the plot to the left side;
  * "review mode", in this mode, the user can move freely the visible window of the plot to analyze the data and even place a red vertical marked by long pressing the view. The plot is refreshing constantly with fresh data as soon they are collected. The button "< Back to RealTime" let the user to switch back to "live mode", than the visible window will be moved to the right side, and locked, as prescribed by "live mode".

##### SmoothingKit #####

The repository include, as a submodule, the SmoothingKit repo and it is a dependency of AmazingPlotApp Xcode project.

The full documentation of this framework, written entirely by me, can be found in README file available on the [SmoothingKit repo](https://bitbucket.org/daniel_sonny/smoothingkit).


##### hacked-Charts #####

To better reach the requirements, in given time frame, I decide to take advantage of the best open source plot library available on iOS. However, I had to hack the library to meet some specific requirements, in particular:

  * I replaced the gesture recognizer to detect the single (short) tap, with the one able to detect long tap.
  * I replaced the code to draw circles (points of the plot), with code to draw pointing down triangles. This to archive the requirement that ask to use triangles to point peaks.
  * It was added a further delegate method to notify to a registered delegate when a pan gesture, on the plot area, began/ended and the direction of the pan (with a method written by me). This to reach the requirement that asking to activate the review mode when the user does a pan to the left (going back on time axis).


### Compatibility ###

AmazingPlotApp is an iOS app, written in [Swift](https://en.wikipedia.org/wiki/Swift_(programming_language))) on last available Xcode version.
It is designed for iOS 10, but will be compatible even with iOS 11.

As requested, the app can run on all size of iOS 10 devices in the range 4"-5.5" in portrait or landscape.
The UI will scale accordingly with no problem.

### Setup Instructions ###

The steps to install the app on an actual devices are:

  0. Clone locally the AmazingPlotApp repo;
  0. Open project in Xcode (in case, It can be downloaded from Mac App Store for free) in cloned folder;
  0. Run it (cmd + R).
  0. Some tweaks to let the app uses an hight frame rate;

### Notes ###

In the early stages, I would like to code a new Framework to plot data to product a closer result, but I had to abort this plan because the time constrains given.

Anyway, I was able to archive the UI/UX requirements with some tweaks on open source plotting Framework of choice, however that was the most difficult part. In particular, the feature to lock the visible part to the left side and unlock it as soon the user scroll to the left the plot.

I made important UX decision that I would like to point out: I disabled intentionally the decelerating while scrolling the plot to let the user be more precise during the review of the data because that makes the scroll of the plot more stiff and precise than usual scrolling views. I think this kind of UX sugar, like decelerating, are good to be implemented when the content shown is already rendered, stable and easily understandable on the screen, like a web page, but unlike a live complex plotting UI.

From my job experience, I learnt that the real time plotting UIs, like the one in AmazingPlotApp, have to be used carefully. This because a real time plot is a way hard to read for the common people (scale of the axes, fast frequency of phenomenons to analyze, people who are not used to the science way to treat data, etc...) so I think, were it's possible, it's better to give the users a more digested data, maybe drawn in a appealing and colorful UI.

The data analysis part of this assignment requires more context about how the data will be used to obtain a more refine result. But I think the result is pretty closer to the requirements and it meet the purpose of this assignment.


### Repo Owner ###

Daniel Sonny Agliardi ([dany901@gmail.com](mailto:dany901@gmail.com), [Linkedin profile](https://www.linkedin.com/in/daniel-sonny-agliardi-136a9596/))
